﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication1
{

    //CLASES DEL PROGRAMA

    class Program
    {
        const int MAXe = 4;//constante que indica el máximo de empresas que podrá almacenar el vector
        const int MAXs= 5;//constante que indica el máximo de servicios que podrá almacenar el vector de servicios perteneciente a cada empresa


       

        class CServicio
        {
            public int codigo_servicio;//variable que almacena el codigo de cada servicio
            public string descripcion;//variable que almacena la descripcion de cada servicio
            public string fecha_act;//variable que almacena la fecha de cada servicio
            public double coste;//variable que almacena el coste de cada servicio
        }
        class CEmpresa
        {
            public int codigo;//variable que almacena el codigo de cada empresa
            public string nom_empresa;//variable que almacena el nombre de cada empresa
            public string nom_representant;//variable que almacena el nombre del representante de cada empresa
            public int num_servicios;//variable que almacena el numero de servicio de cada empresa
            public CServicio[] servicios = new CServicio[MAXs];//vector de servicios pertenenciente a cada empresa

        }

        class CListaEmpresas
        {
            public int numero=0;//variable que almacena el numero de empresas de cada vector
            public CEmpresa[] empreses = new CEmpresa[MAXe];//vector de objetos CEmpresa
        }



        //FUNCIONES CARGAR Y GUARDAR:

        static  int cargar(string nombre_fichero, CListaEmpresas l)
        //esta función carga el fichero de texto con el nombre que recibe por argumento como
        //nombre_fichero y recibe la lista de empresas a la que pertenecen las empresas de este
        //la función devuelve -1 si no ha encontrado el fichero, -2 si el fichero no tiene
        //el formato correcto y 0 si ha ido bien la carga
        {
            int i, j;
            StreamReader F;
            string linea;
            string[] datos_servicio = new string[4];
            
          
            try
            {
                F = new StreamReader(nombre_fichero);
            }
            catch (FileNotFoundException)
            {
                return -1;
            }
            i = 0;

            linea = F.ReadLine();



            while (linea != null && i < MAXe - 1)
            {


                if (linea == "")
                {
                    F.Close();
                    return -2;

                }
                while (linea != "--------")
                {

                    CEmpresa e = new CEmpresa();
                    e.codigo = Convert.ToInt32(linea);
                    e.nom_empresa = F.ReadLine();
                    e.nom_representant = F.ReadLine();
                    e.num_servicios = Convert.ToInt32(F.ReadLine());
                    l.empreses[i] = e;

                    
                    if(e.num_servicios!=0)
                    {

                        j = 0;
                        while (j < l.empreses[i].num_servicios)
                        {
                            CServicio servicio = new CServicio();
                            try
                            {
                                linea = F.ReadLine();
                                datos_servicio = linea.Split(',');

                            }
                            catch (IndexOutOfRangeException)
                            {
                                F.Close();
                                return -2;
                            }

                            int k = 0;
                            try
                            {
                                servicio.codigo_servicio = Convert.ToInt32(datos_servicio[k]);
                                k++;
                                servicio.descripcion = datos_servicio[k];
                                k++;
                                servicio.fecha_act = datos_servicio[k];
                                k++;
                                servicio.coste = Convert.ToDouble(datos_servicio[k]);

                                l.empreses[i].servicios[j] = servicio;




                            }
                            catch
                            {
                                F.Close();
                                return -2;
                            }
                            j++;
                            l.numero++;
                        }
                    }
                        linea = F.ReadLine();
                    
                }
                    linea = F.ReadLine();

                    i++;

                }
            
            F.Close();
            l.numero = i;
            return 0;
        }

       
        static void guardar(string nombre_fichero, CListaEmpresas l)
        //esta función guarda en un fichero de texto cuyo nombre recibe por argumento
        //la información relativa a las empresas pertenecientes a la lista l que también 
        //recibe por argumento
        
        {
            StreamWriter F= new StreamWriter(nombre_fichero);

            int i=0,j=0;

            while(i<l.numero)
            {
                F.WriteLine(l.empreses[i].codigo);
                F.WriteLine(l.empreses[i].nom_empresa);
                F.WriteLine(l.empreses[i].nom_representant);
                F.WriteLine(l.empreses[i].num_servicios);
                j = 0;

                if(l.empreses[i].num_servicios!=0)
                {
                    while (j < l.empreses[i].num_servicios)
                    {
                        F.WriteLine(l.empreses[i].servicios[j].codigo_servicio + ","+ l.empreses[i].servicios[j].descripcion + "," + l.empreses[i].servicios[j].fecha_act + "," + l.empreses[i].servicios[j].coste);
                        j++;

                    }
                }
                F.WriteLine("--------");
                i++;
            }
            F.Close();

      }

        //FUNCIONES RELATIVAS A EMPRESA


        static int alta_empresa(CListaEmpresas l, CEmpresa empresa)
        //esta función añade el objeto empresa que recibe por argumento en una nueva 
        //empresa en la lista de empresas que también recibe por argumento
        //devuelve -2 si la empresa ya existe,-1 si el vector está lleno o 0 si ha ido bien
        {
            int x=0;
            bool trobat=false;
            while (x < l.numero && !trobat)
            {
                if (l.empreses[x].nom_empresa == empresa.nom_empresa)
                    trobat = true;
                else
                    x++;
            }
            if (trobat)
            {
                return -2;
            }
            else
            {

                if (l.numero == MAXe)
                    return -1;

                else
                {
                    l.empreses[l.numero] = empresa;
                    return 0;
                }
            }
        }




        static CEmpresa buscar_empresa(CListaEmpresas l, int codigo_servicio)
        //esta funcion recorre la lista de empresas y devuelve el objeto de la empresa
        //cuyo codigo de un servicio es codigo_servicio. Si el codigo del servicio no 
        // coincide con el codigo de un servicio de alguna empresa devuelve un objeto
        //tipo CEmpresa nulo
        {
            CEmpresa e = new CEmpresa();
            e = null;
            int i = 0,j;

            while (i < l.numero)
            {
                j = 0;
                while (j < l.empreses[i].num_servicios)
                {
                    if (l.empreses[i].servicios[j].codigo_servicio == codigo_servicio)
                        return l.empreses[i];
                    else
                        j++;
                }
                i++;
            }
            return e;

        }



        static int eliminar_empresa(CListaEmpresas list,string nombre_empresa)
        //esta función elimina una empresa de la lista que recibe por argumento segun el nombre que le pasemos por este.
        //devuelve 0 si se ha eliminado o -1 si no existe la empresa que queremos eliminar
        {
            int i = 0;
            while (i < list.numero)
            {
                if (list.empreses[i].nom_empresa == nombre_empresa)
                {
                    list.empreses[i] = null;
                    list.numero--;
                    return 0;
                }
                else
                    i++;
            }
            return -1;
        }

        static void listar_empresa(CListaEmpresas llista, CEmpresa empresa)
        //esta funcion muestra por pantalla toda la información relativa a la empresa de la lista de 
        //empresas que recibe por argumento
        {
            Console.WriteLine("");
            Console.WriteLine("");

            Console.WriteLine("_____________________________________________________");
            Console.WriteLine("");

            Console.WriteLine("Datos de la empresa "+empresa.nom_empresa);
            Console.WriteLine("");
            Console.WriteLine("_____________________________________________________");
            Console.WriteLine("");

            Console.WriteLine("Codigo: " + empresa.codigo);
            Console.WriteLine("Nombre: " + empresa.nom_empresa);
            Console.WriteLine("Nombre del representante legal: " + empresa.nom_representant);
            Console.WriteLine("Numero de servicios: " + empresa.num_servicios);

            Console.WriteLine("");
            Console.WriteLine("Información sobre los servicios:");
            Console.WriteLine("");
            Console.WriteLine("");

            int i = 0;
            try
            {
                while (i < empresa.num_servicios)
                {
                    Console.WriteLine("servicio " + i + ":");
                    Console.WriteLine("");
                    Console.WriteLine("codigo servicio: " + empresa.servicios[i].codigo_servicio);
                    Console.WriteLine("descripcion del servicio: " + empresa.servicios[i].descripcion);
                    Console.WriteLine("Fecha de actualización: " + empresa.servicios[i].fecha_act);
                    Console.WriteLine("Coste del servicio mensual: " + empresa.servicios[i].coste);
                    Console.WriteLine("");
                    i++;

                }
            }
            catch
            {
                Console.WriteLine("");
            }

        }

        //FUNCIONES PARA SERVICIOS


        static int añadir_servicio (CListaEmpresas l, string nombre_empresa, CServicio s)
        //esta funcion añade un servicio nuevo recibido por argumento en la lista de empresas 
        //también recibida por argumento a una empresa cuyo nombre es nombre_empresa recibido por argumento
        //la funcion devuelve 0 si ha ido bien o -1 si no ha encontrado la empresa o -2 si
        //no se pueden añadir más servicios
        {

            int i = 0 ;
            Boolean encontrado = false;
            while (i < l.numero && !encontrado)
            {
                if (l.empreses[i].nom_empresa == nombre_empresa)
                    encontrado = true;
                else
                    i++;
            }

            if (encontrado)
            {
                if (l.empreses[i].num_servicios == MAXs)
                {
                    return -2;
                }
                else
                    l.empreses[i].servicios[l.empreses[i].num_servicios] = s;
                l.empreses[i].num_servicios++;
                return 0;
            }
            else
                return -1;
        }

        static int eliminar_servicio(CListaEmpresas l, int codigo_servicio)
        //esta función elimina un servicio cuyo nombre es recibido por argumento de una empresa que está en la lista 
        //que también recibe por argumento  segun el codigo de este.
        //devuelve 0 si ha ido bien o -1 si no ha encontrado el servicio
        {
            int i = 0, j = 0;
            Boolean encontrado = true;

            while (i < l.numero&&!encontrado)
            {
                while (j < l.empreses[i].num_servicios &&!encontrado)
                {
                    if (l.empreses[i].servicios[j].codigo_servicio == codigo_servicio)
                        encontrado = true;
                    else
                        j++;
                }
                j = 0;
                i++;
            }

            if (encontrado)
            {
                l.empreses[i].servicios[j] = null;
                l.empreses[i].num_servicios--;
                return 0;
            }
            else
                return -1;
        }
        static CServicio buscar_servicio(int codigo_servei, CListaEmpresas l)
        //esta funcion recibe por argumento el codigo de un servicio i la lista de empresas a la que 
        //pertenece, lo busca y,en caso de encontrarlo, devuelve el objeto CServicio con toda la in-
        //formación relativa a este. En caso de no encontrarlo devuelve null.
        {
            int i = 0, j = 0;
            Boolean encontrado = false;

            while (i < l.numero && !encontrado)
            {
                j = 0;
                while (j < l.empreses[i].num_servicios && !encontrado)
                {
                    if (l.empreses[i].servicios[j].codigo_servicio == codigo_servei)
                        encontrado = true;
                    else
                        j++;
                }
                if (!encontrado)
                    i++;
            }
            if (encontrado)
            {
                return l.empreses[i].servicios[j];
            }
            else
                return null;


        }
        




        static int consultar_servicio_caro(CListaEmpresas l)
        //esta funcion busca el servicio con el coste más caro de la lista de empresas que 
        //recibe por argumento y devuelve el codigo de éste.
        {


            int i = 0, j,codigo=-1;
            double maximo = 0;
          
            while (i < l.numero)
            {
                j = 0;
                while (j < l.empreses[i].num_servicios)
                {
                    if (l.empreses[i].servicios[j].coste > maximo)
                    {
                        maximo = l.empreses[i].servicios[j].coste;
                        codigo=l.empreses[i].servicios[j].codigo_servicio;
                        
                    }
                        j++;
                }
                i++;

            }
            return codigo;
            
        }



        static int fijar_fecha(int codigo, CListaEmpresas l)
        //esta funcion asigna la fecha actual del sistema a el servicio cuyo codigo recibe por argumento
        //y el cual pertenece a la lista de empresas l que también recibe por argumento. La función devu_
        //elve zero si ha encontrado el servicio i -1 en caso contrario.
        {
            int i = 0, j=0;
            Boolean encontrado = false;
           
            while (i < l.numero && !encontrado)
            {
                j = 0;
                while (j < l.empreses[i].num_servicios && !encontrado)
                {
                    if (l.empreses[i].servicios[j].codigo_servicio == codigo)
                        encontrado = true;
                    else
                        j++;
                }
                if (!encontrado)
                    i++;
            }
            if (encontrado)
            {
                l.empreses[i].servicios[j].fecha_act = System.DateTime.Now.ToString("dd/MM/yyyy");
                return 0;
            }
            else
                return -1;
        }
            

        static int auxiliar_mostar(int i, CListaEmpresas l)
        //esta funcion es una función auxiliar para mostrar los datos que nos permite saber en que posi-
        //cion del vector de empresa se encuentra la empresa que estamos mostrando con el fin de saber 
        //el mensaje que mostaremos a el usuario por pantalla, en función de si la empresa es la primera,
        //esta en posición media o está al final del vector
        {
            if(i == 0)
            {
                return 1;
            }

            else if (0 < i && i < l.numero-1)
            {
                return 2;

            }
       
            else 
            {
                return 3;
            }
        }


        static void Main(string[] args)
        //este es el programa principal que se ejectutará continuamente cuando usemos la aplicación
        {



            CListaEmpresas l = new CListaEmpresas();
            int opcion;
            bool flag_error = false;
           

            Console.ForegroundColor = ConsoleColor.Blue;
            
            Console.WriteLine("         ██████████████████████████████████████████████████████  ");
            Console.WriteLine("         ██                                                  ██  ");
            Console.Write("             ██        ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Aplicación de Gestión de Empresas");
            Console.ForegroundColor = ConsoleColor.Blue;
            
            
            Console.WriteLine("         ██  ");
            Console.WriteLine("         ██                                                  ██  ");
            Console.WriteLine("         ██████████████████████████████████████████████████████  ");


          

            switch (cargar("fichero.txt", l))
            //este switch servirá para cargar el fichero predeterminado en la aplicación. Se basa en 
            //la función cargar, y en función del valor que devuelva mostraremos por pantalla si el fi-
            //chero se ha cargado correctamente,no se ha encontrado o no tiene el formato correcto.
            {
                case 0:
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Fichero cargado correctamente");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;

                case -1:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No se ha encontrado el fichero");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Beep(300, 250);
                    break;

                case -2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("El fichero no tiene el formato correcto");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Beep(300, 250);
                    break;
            }

            //este es el menú principal que se mostrará al usuario en todo momento

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒");
            Console.WriteLine("");

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\t\tMENÚ PRINCIPAL");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("");
            Console.WriteLine("\t\tIndique opción:");
            Console.WriteLine("");
            Console.WriteLine("\t\t0:\tDatos empresa.");
            Console.WriteLine("\t\t1:\tConsulta servicios.");
            Console.WriteLine("\t\t2:\tAlta empresa.");
            Console.WriteLine("\t\t3:\tBaja empresa.");
            Console.WriteLine("\t\t4:\tAñadir servicio.");
            Console.WriteLine("\t\t5:\tEliminar servicio.");
            Console.WriteLine("\t\t6:\tBuscar empresa donde se encuentra un servicio");
            Console.WriteLine("\t\t7:\tFijar fecha.");
            Console.WriteLine("\t\t8:\tConsultar servicio más caro");
            Console.WriteLine("\t\t9:\tCargar datos");
            Console.WriteLine("\t\t10:\tGuardar datos en fichero");
            Console.WriteLine("\t\t11:\tSalir");

            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒");
            Console.ForegroundColor = ConsoleColor.Gray;

            //inicializamos la variable opcion en 12, ya que es un numero que no pertenece a ninguna opcion
            //de modo que al tratar de convertir el numero no se ejecutará ninguna de estas

            opcion = 12;


            //provamos de convertir el string que escribe el usuario por pantalla a entero
            //en caso que el usuario no escriba un numero se le mostrará el mensaje adecuado
            //por pantalla

            try
            {
                opcion = Convert.ToInt32(Console.ReadLine());
            }

            catch (FormatException)
            {
                flag_error = true;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Se nota que la opción tiene que ser un numero.");
                Console.Beep(300, 250);
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            //en caso que el usuario haya escrito un numero por pantalla pero este no este en el intervalo
            //de opciones del menú, se avisará al usuario con el mensaje correspondiente

            if((opcion<0||opcion>11)&&!flag_error)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Se nota que la opción tiene que ser un numero comprendido entre el 0 y el 11.");
                Console.Beep(300, 250);
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            //ahora construimos un while que será el que permitirá que se muestre constantemente el menú principal 
            //por pantalla mientras no pulsemos una opcion que no sea 11. En el caso de pulsarla, como veremos más
            //a bajo el programa preguntara si se desean guardar los cambios y saldrá de la aplicación.
            
            while (opcion != 11)
            {
                flag_error = false;
                bool flag = false;

                //este switch se ejecutará en funcion del valor de la variable opcion
                switch (opcion)
                {
                    //en el caso 0 se hará un recorrido por el vector de empresas y se mostrará la informa
                    //ción relativa a ellas

                    case 0:
                        Console.Clear();

                        int i = 0, j;
                        string a;

                        while (i < l.numero)
                        {
                             Console.WriteLine("");

                            Console.WriteLine("_____________________________________________________");
                            Console.WriteLine("");

                            Console.WriteLine("Datos de la empresa "+l.empreses[i].nom_empresa);
                            Console.WriteLine("");
                            Console.WriteLine("_____________________________________________________");
                            Console.WriteLine("");
                            
                          
                            Console.WriteLine("Codigo empresa: " + l.empreses[i].codigo);                          
                            Console.WriteLine("Nombre representante: " + l.empreses[i].nom_representant);
                            Console.WriteLine("Numero de servicios: " + l.empreses[i].num_servicios);
                            Console.WriteLine("");
                            Console.WriteLine("");
                            Console.WriteLine("Información de los servicios de la empresa:");
                            Console.WriteLine("");

                            if (l.empreses[i].num_servicios == 0)
                                Console.WriteLine("Esta empresa no tiene servicios");
                            else
                            {

                                j = 0;
                                while (j < l.empreses[i].num_servicios)
                                {
                                    Console.WriteLine("Codigo: " + l.empreses[i].servicios[j].codigo_servicio);
                                    Console.WriteLine("Descripción: " + l.empreses[i].servicios[j].descripcion);
                                    Console.WriteLine("Fecha actualización: " + l.empreses[i].servicios[j].fecha_act);
                                    Console.WriteLine("Coste del servicio: " + l.empreses[i].servicios[j].coste);
                                    Console.WriteLine("");
                                    Console.WriteLine("");
                                    j++;

                                }
                            }
                            Console.WriteLine("--------");

                            //este switch se ejecutará en funcion del valor que devuelva la función auxiliar_mostrar,
                            //la qual nos devuelve 1 si la empresa que estamos mostrando ocupa la primera posición del
                            //vector, 2 si la empresa se encuentra entre el principio y el final, y 3 si se encuentra 
                            //en la última posición

                            switch (auxiliar_mostar(i, l))
                            {
                                case 1:
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine("Pulse s para siguiente empresa o r para volver al menú");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                    Console.WriteLine("");
                                    a = Console.ReadLine();

                                    if (a == "s")
                                    {
                                        //si el usuario pulsa s, aumentamos el valor de la variable i para pasar a mos-
                                        //trar la siguiente empresa

                                        Console.Clear();
                                        i++;
                                        break;
                                    }

                                    else if (a == "r")
                                    {
                                        //en caso de pulsar r, volvemos al menú forzando salir del while asignando a i el
                                        //valor del número de empresas

                                        i = l.numero;
                                        flag = true;
                                        break;
                                    }

                                    else
                                    {
                                        //en caso de no pulsar ninguna tecla disponible se advertirá al usuario por pantalla
                                        //con el mensaje correspondiente

                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("");
                                        Console.Clear();
                                        Console.WriteLine("Se nota que la tecla tiene que ser s o r");
                                        Console.Beep(300, 250);
                                        Console.WriteLine("");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.Beep(300, 250);
                                        break;
                                    }

                                case 2:
                                    //en el caso que la empresa se encuentre en el medio del vector, se le mostrará al
                                    //usuario las opciones que tiene disponibles

                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine("Pulse s para siguiente empresa,a para anterior o r para volver ");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                    Console.WriteLine("");
                                    a = Console.ReadLine();

                                    if (a == "s")
                                    {
                                        //si el usuario pulsa s, aumentamos el valor de la variable i para pasar a mos-
                                        //trar la siguiente empresa
                                        Console.Clear();
                                        i++;
                                        break;
                                    }

                                    if (a == "a")
                                    {
                                        //si el usuario pulsa a,disminuimos el valor de la variable i para pasar a mos-
                                        //trar la anterior empresa
                                        Console.Clear();
                                        i--;
                                    }

                                    if (a == "r")
                                    {
                                        //en caso de pulsar r, volvemos al menú forzando salir del while asignando a i el
                                        //valor del número de empresas
                                        i = l.numero;
                                        flag = true;
                                        break;
                                    }

                                    else
                                    {
                                        //en caso de no pulsar ninguna tecla disponible se advertirá al usuario por pantalla
                                        //con el mensaje correspondiente

                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("");
                                        Console.Clear();
                                        Console.WriteLine("Se nota que la tecla tiene que ser s,a o r");
                                        Console.Beep(300, 250);
                                        Console.WriteLine("");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.Beep(300, 250);
                                        break;
                                    }

                                case 3:
                                    //en el caso que la empresa se encuentre en el final del vector, se le mostrará al
                                    //usuario las opciones que tiene disponibles

                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine("Pulse a para anterior empresa o r para volver al menú");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                    Console.WriteLine("");
                                    a = Console.ReadLine();

                                    if (a == "a")
                                    {
                                        //si el usuario pulsa a,disminuimos el valor de la variable i para pasar a mos-
                                        //trar la anterior empresa
                                        Console.Clear();
                                        i--;
                                        break;
                                    }

                                    else if (a == "r")
                                    {
                                        //en caso de pulsar r, volvemos al menú forzando salir del while asignando a i el
                                        //valor del número de empresas
                                        i = l.numero;
                                        flag = true;
                                        break;
                                    }

                                    else
                                    {
                                        //en caso de no pulsar ninguna tecla disponible se advertirá al usuario por pantalla
                                        //con el mensaje correspondiente

                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("");
                                        Console.Clear();
                                        Console.WriteLine("Se nota que la tecla tiene que ser a o r");
                                        Console.WriteLine("");
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        Console.Beep(300, 250);
                                        break;
                                    }
                            }
                        }
                        break;


                    case 1:
                        //si la opción es uno, mostraremos los datos de los servicios de la empresa cuyo código se lo pediremos al usuario
                        Boolean encontrado;
                        Console.WriteLine("Escriba el codigo de la empresa:");
                        int codigo;

                        //provamos de convertir el codigo a numero entero
                        try
                        {
                            codigo = Convert.ToInt32(Console.ReadLine());


                        }
                        //en caso que el codigo no sea un entero, mostramos el mensaje adecuado por pantalla
                        catch(FormatException)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Los datos introducidos no son correctos");
                            Console.WriteLine("El codigo debe ser un numero entero");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }

                        i = 0;
                        encontrado = false;
                        //aplicamos el esquema de búsqueda para encontrar la empresa según su codigo
                        while (i < l.numero && !encontrado)
                        {
                            if (l.empreses[i].codigo == codigo)
                                encontrado = true;
                            else
                                i++;
                        }
                        if (encontrado)
                        //si encontramos la empresa mostramos la información de todos sus servicios
                        {

                            j = 0;
                            while (j <= l.empreses[i].num_servicios)
                            {
                                Console.WriteLine("servicio " + j + ":");
                                Console.WriteLine("");
                                Console.WriteLine("codigo servicio: " + l.empreses[i].servicios[j].codigo_servicio);
                                Console.WriteLine("descripcion del servicio: " + l.empreses[i].servicios[j].descripcion);
                                Console.WriteLine("Fecha de actualización: " + l.empreses[i].servicios[j].fecha_act);
                                Console.WriteLine("Coste del servicio mensual: " + l.empreses[i].servicios[j].coste);
                                Console.WriteLine("");
                                j++;
                            }

                        }
                        else
                        //si no la encontramos se lo comunicamos al usuario
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("El codigo no coincide con  ninguna empresa");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        break;


                    case 2:
                        //la opcion 2 da de alta una nueva empresa si hay sitio en el vector.

                        if (l.numero == MAXe)
                        //si el vector está lleno lo comunicamos
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("No se pueden añadir más empresas");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }
                        else
                        {
                            //si hay sitio en el vector, creamos un nuevo objeto CEmpresa, lo llenamos con los datos
                            //que nos facilita el usuario y lo añadimos al vector
                            CEmpresa empresa = new CEmpresa();
                            try
                            {

                                Console.WriteLine("Introduzca el codigo de la empresa:");
                                empresa.codigo = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("Escriba el nombre de la empresa:");
                                empresa.nom_empresa = Console.ReadLine();
                                Console.WriteLine("Escriba el nombre del representante:");
                                empresa.nom_representant = Console.ReadLine();
                                Console.WriteLine("Escriba el numero de servicios de la empresa");
                                empresa.num_servicios = Convert.ToInt32(Console.ReadLine());

                                if (empresa.num_servicios > MAXs)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("El numero de servicios tiene que estar entre 0 y " + MAXs);
                                    break;
                                }

                                i = 0;

                                while (i < empresa.num_servicios)
                                {
                                    empresa.servicios[i] = new CServicio();
                                    Console.WriteLine("Escribe el codigo del servicio "+i+":");
                                    empresa.servicios[i].codigo_servicio = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("Escribe la descripcion del servicio " + i + ":");
                                    empresa.servicios[i].descripcion = Console.ReadLine();
                                    Console.WriteLine("Escribe la fecha de actualización " + i + ":");
                                    empresa.servicios[i].fecha_act = Console.ReadLine();

                                    Console.WriteLine("Escribe el coste del servicio " + i + ":");
                                    empresa.servicios[i].coste = Convert.ToDouble(Console.ReadLine());

                                    i++;
                                }

                                if (alta_empresa(l, empresa) == 0)
                                {

                                    l.numero++;
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.WriteLine("Empresa añadida correctamente");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("La empresa ya existe");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                }

                            }
                            catch(FormatException)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("los datos introducidos no son correctos");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }

                            break;
                        }


                    case 3:
                        //la opcion 3 elimina una empresa a partir del nombre y con la funcion eliminar_empresa
                        Console.WriteLine("Introduzca el nombre de la empresa a eliminar");
                        string eliminada = Console.ReadLine();

                        if (eliminada == "")
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Por favor,escriba el nombre de la empresa a eliminar");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        else
                        {

                            if (eliminar_empresa(l, eliminada) == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("La empresa se ha eliminado correctamente");
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("No se ha encontrado la empresa");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }

                        }
                        break;

                    case 4:
                        //esta opcion añade un servicio a la empresa cuyo nombre es introducido por el usuario
                        //el servicio se añade mediante la función añadir_servicio, la cual devuelve 0 si ha ido bien
                        //o -2 si el vector de servicios está lleno

                        Console.WriteLine("Escriba el nombre de la empresa donde quiere añadir el servicio:");
                        string n = Console.ReadLine();

                        //primero miramos si el nombre que nos ha escrito el usuario  es nulo
                        if (n == "")
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("No se puede añadir un servicio a una empresa que no tiene nombre");
                            Console.Beep(250, 300);
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }

                        //si no es nulo el nombre de la empresa, buscamos si coincide con alguna empresa
                        else
                        {
                            
                            i = 0;
                            encontrado=false;

                            while (i < l.numero && !encontrado)
                            {
                                if (l.empreses[i].nom_empresa == n)
                                    encontrado = true;
                                else
                                    i++;
                            }
                            
                            if (encontrado)
                            {
                                //en el caso que hayamos encontrado la empresa, miramos si el vector de servicios
                                //de esa empresa está lleno

                                if (l.empreses[i].num_servicios == MAXs)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("No se pueden añadir más servicios");
                                    Console.Beep(300, 250);
                                    break;
                                }

                                else
                                //si no está lleno, tratamos de convertir los datos del usuario que sean numeros a enteros
                                {

                                    CServicio ser = new CServicio();
                                    try
                                    {
                                        Console.WriteLine("Escriba el codigo del servicio: ");
                                        ser.codigo_servicio = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine("Escriba la descripcion del servicio: ");
                                        ser.descripcion = Console.ReadLine();
                                        Console.WriteLine("Escriba el coste del servicio:");
                                        ser.coste = Convert.ToDouble(Console.ReadLine());

                                        //cabe destacar que al añadir un nuevo servicio se actualizará la propiedad fecha de actualización
                                        //de este con la fecha actual del sistema
                                        ser.fecha_act = System.DateTime.Now.ToString("dd/MM/yyyy");
                                    }
                                    catch(FormatException)
                                    //si algun dato no es correcto, se avisa al usuario
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Alguno de los datos no es correcto");
                                        Console.Beep(300, 250);
                                        Console.ForegroundColor = ConsoleColor.Gray;
                                        break;
                                    }

                                    //si no hemos tenido problemas con los datos tratamos de añadir el servicio
                                    //esta funcion siempre devolverá zero, ya que solo devolvería distinto si el
                                    //vector de servicios estuviera lleno, pero como ya hemos hecho esa comprovación
                                    //anteriormente, sabemos dle cierto que irá bien la alta de servicio
                                    if (añadir_servicio(l, n, ser) == 0)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        Console.WriteLine("Servicio añadido correctamente");
                                        Console.ForegroundColor = ConsoleColor.Red;
                                    }

                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("La empresa no existe");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }

                        }
                        break;

                    case 5:
                        //la opcion cinco elimina un servicio segun el codigo introducido por pantalla.
                        //se llama a la funcion eliminar servicio y esta devuelve 0 si se ha eliminado o 
                        //-1 si no lo ha encontrado

                        Console.WriteLine("Escribe el codigo del servicio que quieres eliminar:");
                        int servicio;
                        try
                        {
                            servicio = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("el codigo del servicio tiene que ser un numero entero");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }

                        if (eliminar_servicio(l, servicio) == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("El servicio ha sido eliminado correctamente");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("El servicio no se ha encontrado");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        break;

                    case 6:
                        //esta opcion nos da la información de la empresa que contiene el servicio cuyo
                        //codigo lo escribe el usuario por pantalla. Para obtener dicha información se 
                        //utiliza la funcion de buscar_empresa

                        Console.WriteLine("Escriba el codigo del servicio");
                        int codi;
                        try
                        {
                            codi = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("El codigo tiene que ser un numero entero");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }


                        CEmpresa e = buscar_empresa(l, codi);

                        if (e == null)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("No existe ninguna empresa que tenga ese servicio");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("codigo empresa: " + e.codigo);
                            Console.WriteLine("nombre empresa: " + e.nom_empresa);
                            Console.WriteLine("nombre representante legal: " + e.nom_representant);
                            Console.WriteLine("numero de servicios: " + e.num_servicios);
                            break;
                        }

                    case 7:
                        //esta opcion asigna la fecha actual del sistema a la propiedad fecha_actualización de un servicio
                        //a partir del codigo que recibe por pantalla y a través de la función DateTime
                        Console.WriteLine("Escribe el codigo del servicio que quieres actualizar con la fecha actual:");
                        int c;
                        try
                        {
                            c = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Se nota que el codigo tiene que ser un numero entero");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }
                        if (fijar_fecha(c, l) == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("La fecha ha sido actualizada correctamente");
                            Console.WriteLine("Fecha actual:" + System.DateTime.Now.ToString("dd/MM/yyyy"));
                            Console.ForegroundColor = ConsoleColor.Green;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("No se ha encontrado el servicio");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }


                        break;

                    case 8:
                        //esta opcion nos mostrará por pantalla los datos del servicio más caro incluyendo la 
                        //empresa a la que pertenece. En primer lugar, mediante la funcion buscar_empresa obtenemos
                        //la empresa a la que pertenece el servicio determinado a partir de su codigo(codigo del ser-
                        //vicio más caro devuelto por la funcion consultar_servicio_car). Comoesta funcion devuelve un objeto 
                        //CEmpresa,mostramos su nombre. 
                        //Luego, creamos un nuevo objeto CServicio y le asignamos el objeto CServicio que nos devuelve la funcion
                        //buscar_sservicio. Finalmente, tenemos un objeto CServicio con la información del servicio
                        //más caro y la mostramos por pantalla.
                       

                        
                        Console.WriteLine("Información del servicio más caro:");
                        Console.WriteLine();
                        Console.WriteLine("Empresa a la que pertenece: \t" + buscar_empresa(l, consultar_servicio_caro(l)).nom_empresa);

                        CServicio s = new CServicio();
                        s = buscar_servicio(consultar_servicio_caro(l), l);

                        Console.WriteLine("Codigo servicio: \t\t" + s.codigo_servicio);
                        Console.WriteLine("Descripcion servicio: \t\t" + s.descripcion);
                        Console.WriteLine("Fecha actualización: \t\t" + s.fecha_act);
                        Console.WriteLine("Coste servicio: \t\t" + s.coste);

                        break;

                    case 9:
                        //esta opcion nos permite cargar un fichero distinto al fichero predeterminado. Su funcionamiento
                        //es igual que el de la carga del mismo
                        Console.WriteLine("Introduzca el nombre del fichero a cargar y su extensión ej: 'exemple.txt'");
                        string nom = Console.ReadLine();
                        if (nom == "")
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("No se puede cargar un fichero sin nombre");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }
                        else
                        {
                            try
                            {
                                cargar(nom, l);
                            }
                            catch(FormatException)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("El nombre o la extension del fichero no són correctos");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                break;
                            }

                            if (cargar(nom, l) == -2)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("El fichero no tiene el formato correcto");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }
                            else if (cargar(nom, l) == -1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("El fichero no se ha encontrado");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                            }
                            else
                                Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Fichero cargado correctamente");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }


                        break;

                    case 10:
                        //esta opcion nos permite guardar en un fichero de texto la información cargada en la memoria.
                        //el usuario introduce el nombre del fichero que se guardará y el programa muestra los mensajes
                        //pertinentes según como ha ido el proceso de salvado

                        Console.WriteLine("Escriba el nombre del fichero en el que guardar");
                        string noms = Console.ReadLine();

                        if (noms == "")
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("No se puede guardar un fichero sin nombre");
                            Console.Beep(300, 250);
                            Console.ForegroundColor = ConsoleColor.Gray;
                            break;
                        }
                        else
                        {
                            try
                            {
                                guardar(noms, l);
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("fichero guardado correctamente");
                                Console.ForegroundColor = ConsoleColor.Gray;
                                break;
                            }
                            catch
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("El nombre o la extension del fichero no són correctos");
                                Console.Beep(300, 250);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                break;
                            }


                        }


                }
                //esta condicion esta exclusivamente para que entren todos los casos excepto el 1,
                //ya que tenemos una propia sentencia para volver al menú desde allí
                if (!flag)
                {
                    Console.WriteLine("");
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Presione cualquier tecla para volver al menú");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.ReadKey();
                }
                Console.Clear();

                Console.WriteLine("");
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒");

                Console.WriteLine("");

                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("\t\tMENÚ PRINCIPAL");
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine("");
                Console.WriteLine("\t\tIndique opción");
                Console.WriteLine("");
                Console.WriteLine("\t\t0:\tDatos empresa.");
                Console.WriteLine("\t\t1:\tConsulta servicios.");
                Console.WriteLine("\t\t2:\tAlta empresa.");
                Console.WriteLine("\t\t3:\tBaja empresa.");
                Console.WriteLine("\t\t4:\tAñadir servicio.");
                Console.WriteLine("\t\t5:\tEliminar servicio.");
                Console.WriteLine("\t\t6:\tBuscar empresa donde se encuentra un servicio");
                Console.WriteLine("\t\t7:\tFijar fecha.");
                Console.WriteLine("\t\t8:\tConsultar servicio más caro");
                Console.WriteLine("\t\t9:\tCargar datos");
                Console.WriteLine("\t\t10:\tGuardar datos en fichero");
                Console.WriteLine("\t\t11:\tSalir");

                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒");
                Console.ForegroundColor = ConsoleColor.Gray;


                opcion = 12;

                try
                {
                    opcion = Convert.ToInt32(Console.ReadLine());
                }

                catch (FormatException)
                {
                    flag_error = true;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Se nota que la opción tiene que ser un numero.");
                    Console.Beep(300, 250);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }

                if ((opcion < 0 || opcion > 11) && !flag_error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Se nota que la opción tiene que ser un numero comprendido entre el 0 y el 11.");
                    Console.Beep(300, 250);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            }

            //Finalmente creamos una sentencia que nos permitirá guardar los cambios hechos antes de salir
            //el funcionamiento se basa en la variable booleana flag_eroor con la que ya hemos tratado ante
            //riormente, que indicará si se produce un error o no, y la variable flag_end, que indicará, si es 
            //true el fin del programa forzando la salida del while.


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("¿Desea guardar los cambios?(S/N)?");
            Console.ForegroundColor = ConsoleColor.Gray;

            string x = Console.ReadLine();
            bool flag_end = false;
            flag_error = false;

            while (!flag_end)
            {
                if (x == "S" || x == "s")
                {
                    Console.WriteLine("");
                    Console.WriteLine("");
                    Console.WriteLine("Introduzca el nombre del fichero en el que guardar los datos");
                    Console.WriteLine("");

                    string d = Console.ReadLine();

                    if (d == "")
                    {
                        Console.WriteLine("");
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("No se puede guardar un fichero sin nombre.");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Beep(300, 250);
                        Console.WriteLine("");
                        Console.WriteLine("Vuelve a intentarlo(S/N)");
                        x = Console.ReadLine();

                        flag_error = true;
                    }
                    if (flag_error == false)
                    {
                        guardar(d,l);
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Los datos se han guardado correctamente en el fichero");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        flag_end = true;
                    }
                }
                else if (x == "N" || x == "n")
                {
                    flag_end = true;
                }
                else
                {
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Se nota que tienes que introducir S para guardar y N para salir sin guardar.");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Beep(300, 250);
                    Console.WriteLine("");
                    Console.WriteLine("Vuelve a intentarlo(S/N)");
                    x = Console.ReadLine();
                }
            }
             
            //Finalmente nos despedimos del usuario
            Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Gracias por usar nuestro programa. Presione cualquier tecla para cerrar.");
            Console.WriteLine("¡Nos vemos!");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.ReadKey();        


                                                              
        }   

    }
}

